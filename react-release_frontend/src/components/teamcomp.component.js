import React, { Component } from "react";

import TeamService from "../services/team.service";
import { PieChart } from "react-minimal-pie-chart";
import { Doughnut, Pie } from "react-chartjs-2";
import AuthService from "../services/auth.service";






export default class BoardAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redusers: [],
      blueUsers: [],
      totalred: 0,
      totalblue: 0,
      currentUser: AuthService.getCurrentUser()
    };



  }

  


  componentDidMount() {
    
    if(AuthService.getCurrentUser()?.username == null){
      this.props.history.push("/home");
  }
  else
  {
    this.retrieveBlueUsers();
    this.retrieveRedUsers();


    console.log("component did mount!")
  }
    
  }

  retrieveBlueUsers(){
      TeamService.getBlueTeam().then(response => {this.setState({
        blueUsers: response.data
      });
      console.log(response.data)
    })
    .catch(e => {
        console.log(e);
    });
  }
  retrieveRedUsers(){
    TeamService.getRedTeam().then(response => {this.setState({
        redusers: response.data
    });
    console.log(response.data)
  })
  .catch(e => {
      console.log(e);
  });
}

  totalScoreofBlue(){
  const SumValue = this.state.blueUsers && this.state.blueUsers.map(
    users => users.users.reduce((a, users) => a += users.score, 0));
  const total = SumValue.reduce((total, score) => total = total + score, 0);
  console.log(total);
  return total;
  }

  totalScoreOfRed(){
    const SumValue = this.state.redusers && this.state.redusers.map(
      users => users.users.reduce((a, users) => a += users.score, 0));
    const total = SumValue.reduce((total, score) => total = total + score, 0);
    console.log(total);
    return total;
  }

  retrieveData(){
    const state = {
      labels: ['Red','Blue'],
      datasets: [
        {
          label: 'Competition',
          backgroundColor: [
            '#c80000',
            '#398AD7'
          ],
          hoverBackgroundColor: [
          '#800000',
          '#2F66A9'
          ],
          data: [this.totalScoreOfRed(),this.totalScoreofBlue()]
        }
      ]
    }
    return state;
  }


 
  render() {
    
    return (
        
        <div>
        <Pie
          data={this.retrieveData()}
          options={{
            title:{
              display:true,
              text:'Competition between teams score',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
      </div>

        
        
    );
  }
}