import React, { Component } from "react";

import TeamService from "../services/team.service";
import AuthService from "../services/auth.service";


export default class BoardAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      total: 0,
      currentUser: AuthService.getCurrentUser()

    };
  }

  componentDidMount() {
    
    this.retrieveUsers();
        
    //   ,
    //   error => {
    //     this.setState({
    //       content:
    //         (error.response &&
    //           error.response.data &&
    //           error.response.data.message) ||
    //         error.message ||
    //         error.toString()
    //     });
    //   }
    // );
    console.log("component did mount!")
  }

  retrieveUsers(){
      TeamService.getBlueTeam().then(response => {this.setState({
          users: response.data
      });
      console.log(response.data)
    })
    .catch(e => {
        console.log(e);
    });
  }

  totalScore(){
  const SumValue = this.state.users && this.state.users.map(
    users => users.users.reduce((a, users) => a += users.score, 0));
  const total = SumValue.reduce((total, score) => total = total + score, 0);
  console.log(total);
  return total;
  }



  render() {   
     const currentUser = this.state.currentUser

    
    return (
        <div>

        <div className="align-center">
        <h4>List of BlueTeam users</h4>
        </div>
        {currentUser!=null ? (
        <div className="col-md-9 mx-auto">
          

          <div className="row">

          <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.users.map(
                                    users => 
                                    <tr key = {users.id}>
                                        <td>{users.users.map(user =>
                                          user.username
                                        )}</td>
                                        <td>{users.users.map(user => 
                                          user.score
                                        )}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>

                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Total Score</th>
                            </tr>
                        </thead>
                        <tbody>
                                    <tr>
                                        <td>
                                        {this.totalScore()}
                                        </td>                             
                                    </tr>
                        </tbody>
                        
                    </table>

          </div>
        </div>
        ):(
          <div className="unauthorized">
            <h1></h1>
            </div>
        )}
        </div>
        
    );
  }
}