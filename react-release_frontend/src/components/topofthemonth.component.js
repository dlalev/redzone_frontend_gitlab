import React, { Component } from "react";
import Logo from "../img/top.png";
import { Grid, Cell } from "react-mdl";
import UserService from "../services/user.service";

export default class TopOfTheMonth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      user: []
    };
  }

  componentDidMount() {
    UserService.getTopOfTheMonth().then(
      response => {
        this.setState({
          user: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (
      // <div className="container">
      //   <header className="jumbotron">
      //     <h3>{this.state.content}</h3>
      //       <h2>{this.state.user.username}</h2>
      //       <h2>{this.state.user.score}</h2>
      //   </header>
      // </div>
      <div style={{width: '100%', margin: 'auto'}}>

      <Grid className="homepage-grid">

        <Cell col={12}>

          <img
            src={Logo}
            alt="top-of-the-month-logo"
            className="homepage-logo"
          />

          <div className="banner-text">
            <h1>{this.state.user.username}</h1>
            <hr/>
            <p>with score of: {this.state.user.score}</p>
          </div>

        </Cell>

      </Grid>

    </div>
      
    );
  }
}