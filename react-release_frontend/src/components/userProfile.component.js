import React, { Component } from "react";
import UserService from "../services/user.service";

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      user: []
    };
  }

  componentDidMount(){
    UserService.getUserByIdLikeAdmin(this.state.id).then(res=>{
        this.setState({user: res.data});
    })
}

  render() {

    return (
      <div className="container">
        <header className="jumbotron">
          <h3>
            <strong>{this.state.user.username}</strong> Profile
          </h3>
        </header>
        <p>
          <strong>Username:</strong>{" "}
          {this.state.user.username}
        </p>
        
        <p>
          <strong>Email:</strong>{" "}
          {this.state.user.email}
        </p>
        <p>
          <strong>Score:</strong>{" "}
          {this.state.user.score}
        </p>
      </div>
    );
  }
}