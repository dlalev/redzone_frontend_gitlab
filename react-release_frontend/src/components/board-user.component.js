import React, { Component } from "react";

import UserService from "../services/user.service";
import css from "../css/UserPanel.css";
import AuthService from "../services/auth.service";

export default class BoardUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      currentUser: AuthService.getCurrentUser()
    };
  }

  componentDidMount() {
    UserService.getUserBoard().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString()
        });
      }
      
    );
  }

  render() {
    const currentUser = this.state.currentUser
    return (
      <div className="container">
        
        <header className="jumbotron">
          <h3>{this.state.content}</h3>
          
        </header>
        {currentUser!=null ? (
          <div class="container-fluid">
          {this.state.content != "Error: Unauthorized"}
    <div class="row">
      <a href="/tic" className="col-lg-4 background-tictactoe align-center text-decoration-none">
           Tic-Tac-Toe
      </a>
        <a href="/clickgame" className="col-lg-4 background-click align-center text-decoration-none">
            Click Game
        </a>
        <a href="/snakegame" class="col-lg-4 background-snake align-center text-decoration-none">
            Snake Game
        </a>
    </div>
    </div>


        ):(
          <div className="unauthorized">
            <h1></h1>
            </div>
        )}
        
      </div>
    );
  }
}