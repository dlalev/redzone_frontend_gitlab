import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";
import UserService from "../services/user.service";
import Logo from "../img/logo.png";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (


      <div style={{width: '100%', margin: 'auto'}}>

        <Grid className="homepage-grid">

          <Cell col={12}>

            <img
              // src="https://www.freelogodesign.org/file/app/client/thumb/bd34a6b0-a65c-4b21-9626-6a8a60ae133c_200x200.png?1606827273130"
              src={Logo}
              alt="logo"
              className="homepage-logo"
            />

            <div className="banner-text">
              <h1>Gaming Web App</h1>
              <hr/>
              <p>Snake Game | Tic Tac Toe | Click It!</p>
              <div className="social-links">
                <a href="http://facebook.com" rel="noopener noreferrer" target="_blank">
                  <i className="fa fa-facebook-square" aria-hidden="true"/>
                </a>
                <a href="https://gitlab.com/dlalev/redzone_frontend_gitlab" rel="noopener noreferrer" target="_blank">
                  <i className="fa fa-github-square" aria-hidden="true"/>
                </a>
              </div>
            </div>

          </Cell>

        </Grid>

      </div>
      // <div className="container">
      //   <header className="jumbotron">
      //     <h3>{this.state.content}</h3>
          
      //   </header>
      // </div>
      
    );
  }
}