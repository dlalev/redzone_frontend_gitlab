import React, { Component } from "react";

import UserService from "../services/user.service";
import AuthService from "../services/auth.service";


export default class BoardAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModeratorBoard: false,
      currentUser: "",
      users: []
    };
  }

  componentDidMount() {
    

    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
      });
    }
    this.retrieveUsers();
        
    //   ,
    //   error => {
    //     this.setState({
    //       content:
    //         (error.response &&
    //           error.response.data &&
    //           error.response.data.message) ||
    //         error.message ||
    //         error.toString()
    //     });
    //   }
    // );
  }

  retrieveUsers(){
      UserService.getAdminBoard().then(response => {this.setState({
          users: response.data
      });
      console.log(response.data);
    })
    .catch(e => {
        console.log(e);
    });
  }

  viewUser(id){
    this.props.history.push(`/api/test/admin/user/${id}`);
  }

  changeRoleToAdmin(id){
    UserService.updateRole(id, "ROLE_ADMIN");
  }
  changeRoleToUser(id){
    UserService.updateRole(id, "ROLE_USER");
  }

  deleteUser(id){
    UserService.deleteUserAsAdmin(id).then(
      res => { this.setState({users: this.state.users.filter(user => user.id !== id)}) }
    )
  }
  updateUser(id){
    this.props.history.push(`/api/test/admin/updateuser/${id}`);
  }



  render() {
    const {currentUser, showModeratorBoard} = this.state;
    return (
      
        <div className="col-md-9 mx-auto">
          <h4>Users List</h4>

          <div className="row">

          <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Score</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.users.map(
                                    user => 
                                    <tr key = {user.id}>
                                        <td>{user.username}</td>
                                        <td>{user.email}</td>
                                        <td>{user.score}</td>
                                        <td>
                                            <button onClick={() => this.updateUser(user.id)} className="btn btn-info">
                                                Update
                                            </button>
                                            <button style={{marginLeft: "10px"}} onClick={() => this.deleteUser(user.id)} className="btn btn-danger">
                                                Delete
                                            </button>
                                            <button style={{marginLeft: "10px"}} onClick={() => this.viewUser(user.id)} className="btn btn-info">
                                                View
                                            </button>
                                            
                                            
                                            <button style={{marginLeft: "10px"}} onClick={showModeratorBoard && (() => this.changeRoleToAdmin(user.id))} className="btn btn-info">
                                            {showModeratorBoard &&(
                                              "Promote" 
                                              )} 
                                               
                                            </button>
                                            <button style={{marginLeft: "10px"}} onClick={showModeratorBoard && (() => this.changeRoleToUser(user.id))} className="btn btn-info">
                                            {showModeratorBoard &&(
                                              "Demote" 
                                              )} 
                                            </button>
                                             
                                            
                                            
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>

          </div>
        </div>
        
    );
  }
}