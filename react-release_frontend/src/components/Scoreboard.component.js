import React, { Component } from "react";

import UserService from "../services/user.service";
import AuthService from "../services/auth.service";

export default class BoardAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      currentUser: AuthService.getCurrentUser()
    };
  }

  componentDidMount() {
    
    this.retrieveUsers();
        
    //   ,
    //   error => {
    //     this.setState({
    //       content:
    //         (error.response &&
    //           error.response.data &&
    //           error.response.data.message) ||
    //         error.message ||
    //         error.toString()
    //     });
    //   }
    // );
  }

  retrieveUsers(){
      UserService.getAllFromScoreboard().then(response => {this.setState({
          users: response.data
      });
      console.log(response.data);
    })
    .catch(e => {
        console.log(e);
    });
  }

  viewUser(id){
    this.props.history.push(`/api/test/admin/user/${id}`);
  }



  render() {
    const currentUser = this.state.currentUser;
    return (
        
          <div>
            <div>
              </div>
              
        <div className="w-100 p-3">
          <div className="w-50 p-3 float-left">
            <a href="/blueteam" class="btn btn-primary w-100">Blue Team</a>
          </div>
          <div className="w-50 p-3 float-left">
            <a href="/redteam" class="btn btn-danger w-100">Red Team</a>
          </div>
        </div>

        <div className="w-25 p-3">
        </div>
        <br></br>
        {currentUser!=null ? (
        <div className="col-md-9 mx-auto">
          <div className="mx-auto">
            <h4>Scoreboard - Top 10 users</h4>
          </div>
          

          <div className="row">

          <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Score</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.users.map(
                                    user => 
                                    <tr key = {user.id}>
                                        <td>{user.username}</td>
                                        <td>{user.score}</td>
                                        <td>
                                            <button style={{marginLeft: "10px"}} onClick={() => this.viewUser(user.id)} className="btn btn-info">
                                                View
                                            </button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>

          </div>
        </div>
        ):(
          <div className="unauthorized">
            <h1></h1>
            </div>
        )}
        </div>
        
        
    );
  }
}