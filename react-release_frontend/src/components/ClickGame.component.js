import React, { Component } from "react";
import AuthService from "../services/auth.service";

import UserService from "../services/user.service";


export default class ClickGame extends Component {
  constructor(props) {
    super(props);

    this.state = {
      score: 0,
      currentUser: AuthService.getCurrentUser()
    };
  }

  componentDidMount(){
    if(AuthService.getCurrentUser()?.username == null){
      this.props.history.push("/home");
    }
    else
    {
      console.log("component did mount ");
    }
  }

  IncrementItem = () => {
    this.setState({ score: this.state.score + 1 });
  }

  SaveScore = () =>{

    UserService.updateScore(this.state.currentUser.id, this.state.score)
    this.setState({ score: 0})
  }




  render() {
    return (
      <div className="container">
       <button className="btn btn-outline-warning btn-customSize" onClick={this.IncrementItem}>
            Click To Score!
        </button>
        <div className="align-center">
            <h4>score: {this.state.score}</h4>
        </div>
        <button className="btn btn-danger btn-lg btn-block" onClick={this.SaveScore}>
            Save!
        </button>
        
      </div>
    );
  }
}