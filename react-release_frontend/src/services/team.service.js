import axios from 'axios';
import authHeader from './auth-header';

const API_URL = process.env.REACT_APP_API_KEY;

class TeamService {
  getAll() {
    console.log("getAll done");
    return axios.get(API_URL + 'all');
    
  }

  getBlueTeam() {
    return axios.get(API_URL + 'blueteamm');
  }

  getRedTeam() {
    return axios.get(API_URL + 'redteamm');
  }

  addToBlueTeam(id) {
    return axios.put(API_URL + 'addtoblue/' + id, { headers: authHeader() });
  }

  addToRedTeam(id) {
    return axios.put(API_URL + 'addtored/' + id, { headers: authHeader() });
  }
  

}

export default new TeamService();