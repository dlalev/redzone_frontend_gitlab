import axios from 'axios';
import authHeader from './auth-header';
const API_URL = process.env.REACT_APP_API_KEY;


class ForgotPass {

    processForgotPass(){
        return axios.post(API_URL + '/forgot_password');
    }
  
}

export default new ForgotPass();