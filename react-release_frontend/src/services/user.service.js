import axios from 'axios';
import authHeader from './auth-header';
const API_URL = process.env.REACT_APP_API_KEY;
const ADMIN_API_URL = "admin";
const USER_API_URL = "user/";

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'getall');
  }

  getUserBoard() {
    return axios.get(API_URL + USER_API_URL, { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + ADMIN_API_URL, { headers: authHeader() });
  }

  getUserByIdLikeAdmin(userId) {
        return axios.get(API_URL + ADMIN_API_URL + "/" + USER_API_URL + userId, { headers: authHeader() });
  }
  deleteUserAsAdmin(userId) {
        return axios.delete(API_URL +  ADMIN_API_URL + "/" + USER_API_URL + userId, { headers: authHeader() });
  }
  updateUserAsAdmin(user, userId){
        return axios.put(API_URL + ADMIN_API_URL +"/updateuser/" + userId,user, { headers: authHeader() })
  }
  getTopOfTheMonth(){
         return axios.get(API_URL + "scoreboard/topofthemonth");
  }
  getAllFromScoreboard(){
        return axios.get( API_URL + "scoreboard/getall", { headers: authHeader() });
  }
  updateScore(userId,score){
    return axios.put(API_URL + "increasescore/" + userId + "/" + score, { headers: authHeader() })
  }
  updateRole(userId, role){
    return axios.put(API_URL + "updateRole/" + userId + "/" + role, { headers: authHeader() })
  }

  
}

export default new UserService();