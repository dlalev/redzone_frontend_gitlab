import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import SockJsClient from 'react-stomp';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardAdmin from "./components/board-admin.component";
import UserProfile from "./components/userProfile.component";
import UpdateUser from "./components/updateUser.component";
import TopOfTheMonth from "./components/topofthemonth.component";
import Scoreboard from "./components/Scoreboard.component";
import BlueTeamComponent from "./components/blueteam.component";
import RedTeamComponent from "./components/redteam.component";
import ChatComponent from "./components/chat.component";
import PieChart from "./components/teamcomp.component"
import ClickGame from "./components/ClickGame.component"
import ForgotPassword from "./components/forgotpass.component";
import SnakeGame from "./components/snakegame.component";
import TicTacToe from "./components/TicTacToe.component";


require('dotenv').config();


class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showAdminBoard: false,
      showModeratorBoard: false,
      currentUser: "",
      serverTime: null,
      messages: [],
      typedMessage: "",
      name: ""
    };
  }

  setName = (name) => {
    console.log(name);
    this.setState({name: name});
  };

  componentDidMount() {
    console.log("Component did mount");

    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR")
      });

      
    }
    
  }

  logOut() {
    AuthService.logout();
  }



  render() {
    const { currentUser, showAdminBoard, showModeratorBoard } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark header-color">
          <Link to={"/"} className="navbar-brand">
            redZone
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/home"} className="nav-link">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/topofthemonth"} className="nav-link">
                Top of the month
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/tic"} className="nav-link">
                Tic-Tac-Toe
              </Link>
            </li>
            
            
            
            


            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/admin"} className="nav-link">
                  Admin Board
                </Link>
              </li>
            ) || showModeratorBoard && (
              <li className="nav-item">
                <Link to={"/admin"} className="nav-link">
                  Admin Board
                </Link>
              </li>
            )}



            {currentUser &&(
              <li className="nav-item">
                <Link to={"/user"} className="nav-link">
                  User
                </Link>
              </li>
            )}
            {currentUser && (
              <li className="nav-item">
                <Link to={"/scoreboard"} className="nav-link">
                  Scoreboard
                </Link>
              </li>
            )}
            {currentUser && (
              <li className="nav-item">
                <Link to={"/piechart"} className="nav-link">
                  Chart of Competition
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  LogOut
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div>
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path={"/topofthemonth"} component={TopOfTheMonth} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/scoreboard" component={Scoreboard} />
            <Route exact path="/blueteam" component={BlueTeamComponent} />
            <Route exact path="/redteam" component={RedTeamComponent} />
            <Route path="/user" component={BoardUser} />
            <Route path="/admin" component={BoardAdmin} />
            <Route exact path="/api/test/admin/user/:id" component={UserProfile} />
            <Route exact path="/api/test/admin/updateuser/:id" component={UpdateUser} />
            <Route path="/websocket-chat" component={ChatComponent} />
            <Route path="/piechart" component={PieChart} />
            <Route path="/clickgame" component={ClickGame} />
            {/* <Route exact path="/forgot_password" component={ForgotPassword} /> */}
            <Route exact path="/snakegame" component={SnakeGame} />
            <Route path="/tic" component={TicTacToe} />


            

            {/* <Route exact path="/api/chat" component={Chat} /> */}
          </Switch>
        
        </div>
        {currentUser &&(
        <a href="websocket-chat">
        <div className="footer">
            
        </div>
        </a>
        )}
        
      </div>
    );
  }
}

export default App;