describe('Scoreboard', function(){
    
    it('ViewScoreBoard', function(){
        cy.Login()
        cy.get('.nav-link').contains('Scoreboard').should('be.visible').click()
        cy.get('h4').contains('Scoreboard - Top 10 users').should('be.visible')
        cy.get('td').contains('test').should('be.visible')
    })

    it('ViewBlueTeam', function(){
        cy.Login()
        cy.get('.nav-link').contains('Scoreboard').should('be.visible').click()
        cy.get('h4').contains('Scoreboard - Top 10 users').should('be.visible')
        cy.get('.btn-primary').contains('Blue Team').should('be.visible').click()
        cy.get('h4').contains('List of BlueTeam users').should('be.visible')
    })

    it('ViewRedTeam', function(){
        cy.Login()
        cy.get('.nav-link').contains('Scoreboard').should('be.visible').click()
        cy.get('h4').contains('Scoreboard - Top 10 users').should('be.visible')
        cy.get('.btn-danger').contains('Red Team').should('be.visible').click()
        cy.get('h4').contains('List of RedTeam users').should('be.visible')
    })


    


})