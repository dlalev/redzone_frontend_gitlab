//test for the login
describe('Sign up', function(){
    it('Username already taken', function(){
        cy.visit('http://localhost:3000/register')
        cy.title().should('eq','RedZone App')
        cy.get('input[type="text"]').first().type('test')
        cy.get('input[name="email"]').first().type('test@gmail.com')
        cy.get('input[type="password"]').type('123456')
        cy.get('.btn').contains('Sign Up').should('be.visible').click()
        cy.get('.alert').contains('Error: Username is already taken!').should('be.visible')
    })

    it('Email already taken', function(){
        cy.visit('http://localhost:3000/register')
        cy.title().should('eq','RedZone App')
        cy.get('input[type="text"]').first().type('testovete')
        cy.get('input[name="email"]').first().type('test@gmail.com')
        cy.get('input[type="password"]').type('123456')
        cy.get('.btn').contains('Sign Up').should('be.visible').click()
        cy.get('.alert').contains('Error: Email is already in use!').should('be.visible')
    })

    it('Username error', function(){
        cy.visit('http://localhost:3000/register')
        cy.title().should('eq','RedZone App')
        cy.get('input[type="text"]').first().type('no')
        cy.get('input[name="email"]').first().type('test@gmail.com')
        cy.get('.alert').first().contains('The username must be between 3 and 20 characters.').should('be.visible')
    })

    it('Email error', function(){
        cy.visit('http://localhost:3000/register')
        cy.title().should('eq','RedZone App')
        cy.get('input[name="email"]').first().type('test')
        cy.get('input[type="text"]').first().type('test')
        cy.get('.alert').first().contains('This is not a valid email.').should('be.visible')
    })

    it('Password error', function(){
        cy.visit('http://localhost:3000/register')
        cy.title().should('eq','RedZone App')
        cy.get('input[type="password"]').first().type('test')
        cy.get('input[name="email"]').first().type('test')
        cy.get('.alert').first().contains('The password must be between 6 and 40 characters.').should('be.visible')
    })

    it('Field required', function(){
        cy.visit('http://localhost:3000/register')
        cy.title().should('eq','RedZone App')
        cy.get('.btn').contains('Sign Up').should('be.visible').click()
        cy.get('.alert').first().contains('This field is required!').should('be.visible')
    })




    // it('Register success', function(){
    //     cy.visit('http://localhost:3000/register')
    //     cy.title().should('eq','RedZone App')
    //     cy.get('input[type="text"]').first().type('testover1')
    //     cy.get('input[name="email"]').first().type('testover1@gmail.com')
    //     cy.get('input[type="password"]').type('123456')
    //     cy.get('.btn').contains('Sign Up').should('be.visible').click()
    //     cy.get('.alert-success').contains('User registered successfully! Enjoy playing!').should('be.visible')
    // })

})