describe('Games', function(){
    
    it('clickGame', function(){
        cy.Login()
        cy.get('.nav-link').contains('User').should('be.visible').click()
        cy.get('h3').contains('Choose which game to play').should('be.visible')
        cy.get('.background-click').contains('Click Game').should('be.visible').click()
        cy.get('.btn-customSize').contains('Click To Score').should('be.visible').click()
        cy.get('h4').contains('score: 1').should('be.visible')
    })

    it('SnakeGame', function(){
        cy.Login()
        cy.get('.nav-link').contains('User').should('be.visible').click()
        cy.get('h3').contains('Choose which game to play').should('be.visible')
        cy.get('.background-snake').contains('Snake Game').should('be.visible').click()
        cy.contains('SCORE: 0').should('be.visible')
    })


    


})