describe('AdminBoard', function(){
    
    it('AdminBoard', function(){
        cy.Login()
        cy.get('.nav-link').contains('Admin Board').should('be.visible').click()
        cy.get('h4').contains('Users List').should('be.visible')
        cy.get('td').contains('moderator').should('be.visible')
        cy.get('.btn-info').contains('View').should('be.visible')

        
    })

    it('UpdatePlayer', function(){
        cy.Login()
        cy.get('.nav-link').contains('Admin Board').should('be.visible').click()
        cy.get('h4').contains('Users List').should('be.visible')
        cy.get('td').contains('moderator').should('be.visible')
        cy.get('.btn-info').first().contains('Update').should('be.visible').click()
        cy.get('.btn-primary').contains('Update').should('be.visible')

    })

    it('ViewPlayer', function(){
        cy.Login()
        cy.get('.nav-link').contains('Admin Board').should('be.visible').click()
        cy.get('h4').contains('Users List').should('be.visible')
        cy.get('td').contains('moderator').should('be.visible')
        cy.get('.btn-info').contains('View').should('be.visible').click()
        cy.contains('moderator Profile').should('be.visible')
    })



    


})