Cypress.Commands.add("Login", () => {
    cy.visit('http://localhost:3000/login')
        cy.title().should('eq','RedZone App')
        cy.get('input[type="text"]').type('test')
        cy.get('input[type="password"]').type('123456')
        cy.get('.btn').contains('Login').should('be.visible').click()
        cy.get('h3').contains('test Profile').should('be.visible')
        cy.contains('test@gmail.com').should('be.visible')
})